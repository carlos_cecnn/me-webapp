import { Component, OnInit } from '@angular/core';
import {Contact,ContactService} from '../../services/contact.service'
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactInfo : Contact[]=[]

  constructor(private contactService:ContactService) { }

  ngOnInit() {
    this.contactInfo=this.contactService.getContact()
  }

}
