import { Component, OnInit } from "@angular/core";
import { Path, MyPathService } from "../../services/my-path.service";
@Component({
  selector: "app-my-path",
  templateUrl: "./my-path.component.html",
  styleUrls: ["./my-path.component.css"],
})
export class MyPathComponent implements OnInit {
  pathContent : Path[]=[]

  constructor(private pathService:MyPathService) {}

  ngOnInit(){
    this.pathContent=this.pathService.getPath()
    console.log(this.pathContent)
  }
}
