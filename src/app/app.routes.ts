
import { RouterModule, Routes } from '@angular/router';

import {MyselfComponent} from './components/myself/myself.component'
import {MyPathComponent} from './components/my-path/my-path.component'
import {ContactComponent} from './components/contact/contact.component'
import {ArtComponent} from './components/art/art.component'
import {ResumeComponent} from './components/resume/resume.component'

const APP_ROUTES: Routes = [

  //Se inicializan todas las rutas que vamos a utilizar.
  { path: '', component: MyselfComponent },
  { path: 'path', component: MyPathComponent},
  { path: 'contact', component:ContactComponent},
  { path: 'art', component: ArtComponent},
  { path:'resume', component:ResumeComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }

];

//Se tiene que importar APP_ROUTING en el archivo app.module.ts como un import,
//y dentro del arreglo de imports[]
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
