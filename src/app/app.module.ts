import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { APP_ROUTING } from "./app.routes";

import { AppComponent } from "./app.component";
import { NavBarComponent } from "./components/common/nav-bar/nav-bar.component";
import { MyselfComponent } from "./components/myself/myself.component";
import { MyPathComponent } from "./components/my-path/my-path.component";
import { ContactComponent } from "./components/contact/contact.component";
import { ArtComponent } from "./components/art/art.component";
import { ResumeComponent } from "./components/resume/resume.component";
import { ContactService } from "./services/contact.service";
import { MyPathService } from "./services/my-path.service";

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MyselfComponent,
    MyPathComponent,
    ContactComponent,
    ArtComponent,
    ResumeComponent,
  ],
  imports: [BrowserModule, APP_ROUTING],
  providers: [ContactService, MyPathService],
  bootstrap: [AppComponent],
})
export class AppModule {}
