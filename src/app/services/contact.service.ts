import {Injectable} from "@angular/core"
@Injectable()
export class ContactService{
    private contactInfo: Contact[]=[
        {
            class:"github",
            icon:"fa-github",
            title:"Github",
            desc:"You can find me on Github as @carlos-cecnn12. Even though I use it a lot, it's mostly for school projects or assignments, and code I developed on Hackathons.",
            button:{
                link:"https://github.com/carlos-cecnn12",
                text:"checkCode()"
            }
        },{
            class:"mail",
            icon:"fa-envelope-o",
            title:"Email me",
            desc:"I'm constantly checking my email because I don't like having unread mail notifications on my phone, so I'll try to answer ASAP.",
            button:{
                link:"mailto:carloscarnog@gmail.com",
                text:"Send me an email"
            }
        },{
            class:"soundcloud",
            icon:"fa-soundcloud",
            title:"Soundcloud",
            desc:"Music has been a hobby of mine since I was a kid, but I got more into producing it lately, so here you can check some experiments and compositions of mine.",
            button:{
                link:"https://soundcloud.com/carlos-cecnn12",
                text:"Hey, listen!"
            }
        },{
            class:"insta",
            icon:"fa-instagram",
            title:"Instagram",
            desc:"I try to upload photos into my account every now and then, but it's not as frecuently as I would like to since I don't always have the resources to develop the film",
            button:{
                link:"https://www.instagram.com/analog_cecnn/",
                text:"Let me see"
            }
        }
    ]

    getContact():Contact[]{
        return this.contactInfo
    }

}

export interface Contact{
    class: string;
    icon: string;
    title: string;
    desc:string;
    button: Button;
}
export interface Button{
    link:string;
    text:string;
}