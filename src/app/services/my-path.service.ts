import { Injectable } from "@angular/core";
@Injectable()
export class MyPathService {
  private pathContent: Path[] = [
    {
      title: "FRC Team Buluk #3472",
      desc: `Here I got my first approach to programming, 
      I learned the basics and developed soft skills related to negotiation and teamwork.
      I played major roles here, got into the drive team and afterwards, I mentored it.
      In addition, I was captain for the programming area in the 2016th season during my senior year of highschool.`,
      time: "Where it all began",
      id_time: "begin",
    },
    {
      title: "Hackathons",
      desc: `Hackathons have been important for me since the first time I heard about them. 
      A lot of my personal and professional development has been thanks to the involvement I've
      had throughout them.
      Furthermore, I've helped with the executive organization and as staff on some hackathons, giving me the experience as an organizer also.`,
      id_time: "hacks",
      time: "How I got engaged",
    },
    {
      title: "GE's Summer Internship",
      desc: `I was part of the first Summer Internship program in the Mexico City office. I was assigned to the Current
      bussiness, where my team developed a project throughout seven weeks. We were introduced to methodologies like SCRUM, and
      Design Thinking to have a better undestanding on how the company carries out projects. My team won both the best project insite and nationally.`,
      time: "My first intership",
      id_time: "ge_intern",
    },
    {
      title: "Facebook Global Hackathon Finalist",
      desc: `After winning the 2018 HackMTY, my team got a ticket to participate in the Facebook Global
      Hackathon in the headquarters of the Menlo Park. It was so nice taking part of this hackathon, I learned a lot of
      things and got to meet a lot of incredible people with amazing ideas to improve the world we live in.`,
      time: "Completing a milestone",
      id_time: "first_milestone",
    },
    {
      title: "Student Internship at Honeywell",
      desc: `Honeywell is the place where I got to really invest time to learn new things while implementing them on
       real life projects. I'm really thankful for this experience since it helped me realize where to head my path because
       it sparked my interest and passion on web developing.`,
      time: "Following the path",
      id_time: "hon_coop",
    },
    {
      title: "Park Good Inc.",
      desc: `I've never seen myself owning a bussiness, but in our college we have to take courses on entrepeneurship. 
      While we were having a chat, some friends and I came up with the idea to revolutionize the way we park by adding
      computer vision and artificial intelligence to the parking lots. This idea blew up and won us a place at incMTY,
      the biggest entrepeneurship forum of Latin America.`,
      time: "Another milestone on the way",
      id_time: "other_milestone",
    },
    {
      title: "Mentoring at SALÓN",
      desc: `After my student internship at Honeywell came to an end, I wasn't interested in getting another internship somewhere
       else, but I really wanted to continue developing. A friend of mine came up with the idea to bring people near to web design and development.
        We designed a course for people to learn the basics on this matter and mentored them in their process to create their own web pages.`,
      time: "Spreading knowledge",
      id_time: "salon",
    },
    {
      title: "The path goes on",
      desc: `I'm still figuring out a lot of things while on the way, things don't end here...`,
      time: "To be continued...",
      id_time: "last",
    },
  ];
  getPath(): Path[] {
    return this.pathContent;
  }
}

export interface Path {
  title: string;
  desc: string;
  time: string;
  id_time: string;
}
